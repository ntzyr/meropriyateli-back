<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Collection;

trait AllowFilterApply
{
    /**
     * @param array $request
     * @return Collection
     */
    public function filter(array $request): Collection
    {
        $this->setFilter($this);

        return $this->apply($request);
    }
}
