<?php

namespace App\Filters;

use App\Event;
use App\Filters\AbstractFilter;
use App\Traits\AllowFilterApply;
use Illuminate\Database\Eloquent\Builder;

class EventFilter extends AbstractFilter
{
    use AllowFilterApply;

    /**
     * @var array
     */
    protected $fields = [
        'price', 'date_start', 'latitude', 'longitude', 'organisator'
    ];

    /**
     * @var string 
     */
    protected $model = Event::class;

    /**
     * @param string $value
     * @return Builder
     */
    protected function date_startFilter(string $value): Builder
    {
        return $this->builder
            ->whereBetween('date_start', explode(',', $value));
    }

    /**
     * @param string $value
     * @return Builder
     */
    protected function latitudeFilter(string $value): Builder
    {
        $value_arr = explode(':', $value);
        return $this->builder
            ->whereBetween('latitude', [(float)$value_arr[0], (float)$value_arr[1]]);
    }

    /**
     * @param string $value
     * @return Builder
     */
    protected function longitudeFilter(string $value): Builder
    {
        $value_arr = explode(':', $value);
        return $this->builder
            ->whereBetween('longitude', [(float)$value_arr[0], (float)$value_arr[1]]);
    }

    /**
     * @param string $value
     * @return Builder
     */
    protected function priceFilter(string $value): Builder
    {
        return $this->builder
            ->whereBetween('price', explode('-', $value));
    }

    protected function organisatorFilter(string $value): Builder
    {
        return $this->builder
            ->where('organisator_id', $value);
    }
}
