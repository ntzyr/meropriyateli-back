<?php

namespace App\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractFilter
{
    /**
     * @var Builder
     */
    protected $builder;

    /**
     * @var self
     */
    protected $filter;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * AbstractFilter constructor.
     */
    public function __construct()
    {
        $this->setUp();
    }

    /**
     * @param array $request
     * @return Collection
     */
    protected function apply(array $request): Collection
    {
        foreach ($this->fields as $field) {
            if (array_key_exists($field, $request)) {
                $value = $request[$field];

                try {
                    call_user_func([$this->filter, $field . 'Filter'], $value);
                } catch (\ErrorException $e) {
                    $this->defaultCall($field, $value);
                }
            }
        }

        return $this->builder->get();
    }

    /**
     * @param string $field
     * @param string $value
     * @return Builder
     */
    protected function defaultCall(string $field, string $value): Builder
    {
        return $this->builder
            ->where($field, $value);
    }

    /**
     * @param Builder $builder
     * @return void
     */
    protected function setBuilder(Builder $builder): void
    {
        $this->builder = $builder;
    }

    /**
     * @param self $filter
     * @return void
     */
    protected function setFilter(self $filter): void
    {
        $this->filter = $filter;
    }

    /**
     * @return void
     */
    private function setUp(): void
    {
        $this->builder = $this->model::query();
    }
}
