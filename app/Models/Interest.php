<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $fillable = [
        'slug', 'label'
    ];

    public function events() {
        return $this->belongsToMany('App\Event');
    }

    public function users() {
        return $this->belongsToMany('App\Interest');
    }
}
