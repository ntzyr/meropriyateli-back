<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'text', 'like', 'event_id', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'organisator_id');
    }

    public function event()
    {
        return $this->belongsTo('App\User');
    }
}
