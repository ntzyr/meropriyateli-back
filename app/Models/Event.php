<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $casts = [
        'price' => 'int',
        'latitude' => 'float',
        'longitude' => 'float',
    ];

    protected $fillable = [
        'thumbnail', 'slug', 'title', 'description', 'latitude', 'longitude', 'date_start', 'date_end', 'price', 'organisator_id'
    ];

    public function interests()
    {
        return $this->belongsToMany(Interest::class);
    }

    public function media()
    {
        return $this->hasMany(Media::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function subscribers()
    {
        return $this->belongsToMany(User::class, 'subscriptions');
    }

    public function organisator()
    {
        return $this->hasOne(User::class, 'id', 'organisator_id');
    }

    public function getShortDateAttribute()
    {
        return date('Y-m-d', strtotime($this->date_start));
    }
    public function getStatusAttribute()
    {
        if($this->short_date > date('Y-m-d')) {
            return true;
        } else {
            return false;
        }
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
