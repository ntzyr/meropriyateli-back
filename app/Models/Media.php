<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'file', 'slug', 'event_id', 'user_id'
    ];

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function author()
    {
        return $this->belongsTo('App\User');
    }
}
