<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'text', 'from_id', 'to_id'
    ];

    public function sender()
    {
        return $this->hasOne('App\User', 'from_id');
    }

    public function recipient()
    {
        return $this->hasOne('App\User', 'to_id');
    }
}
