<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Date;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'thumbnail' => $this->thumbnail,
            'title' => $this->title,
            'description' => $this->description,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'price' => $this->price,
            'organisator' => $this->organisator,
            'interests' => $this->interests,
            'media' => $this->media,
            'reviews' => $this->reviews,
            'reviews_count' => count($this->reviews),
            'subscribers' => count($this->subscribers),
            'short_date' => $this->short_date,
            'status' => $this->status
        ];
    }
}
