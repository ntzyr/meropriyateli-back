<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Filters\EventFilter;
use App\Http\Resources\EventResource;
use App\Models\Interest;
use App\Models\User;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display all event by filtering
     * @return \Illuminate\Http\Resources\Json\EventResource
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter');

        switch ($filter) {
            case 'events':
                return EventResource::collection(
                    (new EventFilter())->filter($request->all()))
                        ->sortBy('short_date')
                        ->groupBy('short_date');
                break;
            case 'subscribtions':
                $user = $request->input('user');
                if($user) {
                    $user = User::find($user);
                }
                return EventResource::collection($user->subscriptions)
                        ->sortBy('short_date')
                        ->groupBy('short_date');
                break;
            case 'popular':
                return EventResource::collection(
                    (new EventFilter())->filter($request->all()))
                        ->sortByDesc('subscribers')
                        ->groupBy('short_date');
                break;
            default:
                return EventResource::collection(
                    (new EventFilter())->filter($request->all()))
                        ->sortBy('short_date')
                        ->groupBy('short_date');
                break;
        }
    }

    public function markers(Request $request)
    {
        return EventResource::collection(
            (new EventFilter())->filter($request->all())
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = $request->isMethod('put') ? Event::findOrFail($request->id) : new Event;

        if($event->save($request->all())) {
            return new EventResource($event);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        if($event->slug) {
            $event = Event::where('slug', $event->slug)->first();
        }
        
        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        if($event->delete()) {
            return new EventResource($event);
        }
    }
}
