<?php

use Faker\Generator as Faker;

$factory->define(App\Message::class, function (Faker $faker) {
    return [
        'text' => $faker->text,
        'from_id' => $faker->numberBetween(1, App\User::count()),
        'to_id' => $faker->numberBetween(1, App\User::count())
    ];
});
