<?php

use Faker\Generator as Faker;

$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'text' => $faker->text(),
        'like' => $faker->boolean(50),
        'event_id' => $faker->numberBetween(1, App\Event::count()),
        'user_id' => $faker->numberBetween(1, App\User::count())
    ];
});
