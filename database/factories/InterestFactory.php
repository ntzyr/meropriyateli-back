<?php

use Faker\Generator as Faker;

$factory->define(App\Interest::class, function (Faker $faker) {
    $interestName = $faker->words();
    return [
        'slug' => implode("-", $interestName),
        'label' => ucfirst(implode(" ", $interestName))
    ];
});
