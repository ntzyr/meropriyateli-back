<?php

use Faker\Generator as Faker;

$factory->define(App\Media::class, function (Faker $faker) {
    $mediaSlug = $faker->words();
    return [
        'file' => $faker->imageUrl(1920,1080),
        'slug' => implode("-", $mediaSlug),
        'event_id' => $faker->numberBetween(1, App\Event::count()),
        'user_id' => $faker->numberBetween(1, App\User::count())
    ];
});
