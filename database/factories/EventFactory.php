<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    $eventTitle = $faker->words();
    return [
        'slug' => implode("-", $eventTitle),
        'title' => ucfirst(implode(" ", $eventTitle)),
        'description' => $faker->paragraph(3),
        'thumbnail' => $faker->imageUrl(328,168),
        'latitude' => $faker->latitude(),
        'longitude' => $faker->longitude(),
        'date_start' => $faker->dateTimeBetween('now', '+1 months'),
        'date_end' => $faker->dateTimeBetween('now', '+1 months'),
        'price' => $faker->randomFloat(2, 100, 4000),
        'organisator_id' => $faker->numberBetween(1, App\User::count())
    ];
});

$factory->state(App\Event::class, 'ended', function (Faker $faker) {
    return [
        'date_start' => $faker->dateTimeBetween('-1 months', 'now'),
        'date_end' => $faker->dateTimeBetween('-1 months', 'now'),
    ];
});

$factory->state(App\Event::class, 'spb', function (Faker $faker) {
    return [
        'latitude' => $faker->randomFloat(10, 59.79008287436167, 60.04923100645964),
        'longitude' => $faker->randomFloat(10, 30.093679351838432, 30.588064117463432),
    ];
});
