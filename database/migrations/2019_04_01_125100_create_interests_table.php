<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug')->unique();
            $table->string('label');
            
            $table->timestamps();
        });

        Schema::create('interest_user', function (Blueprint $table) {
            $table->bigInteger('interest_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();

            $table->foreign('interest_id')
                ->references('id')->on('interests')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

        Schema::create('event_interest', function (Blueprint $table) {
            $table->bigInteger('event_id')->unsigned();
            $table->bigInteger('interest_id')->unsigned();

            $table->foreign('event_id')
                ->references('id')->on('events')
                ->onDelete('cascade');
            $table->foreign('interest_id')
                ->references('id')->on('interests')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interests');
    }
}
