<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug')->unique()->index();
            $table->string('thumbnail')->nullable();
            $table->string('title');
            $table->text('description');
            $table->float('latitude');
            $table->float('longitude');
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->decimal('price', 8, 2)->nullable();

            $table->bigInteger('organisator_id')->unsigned();
            $table->foreign('organisator_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            
            $table->timestamps();
        });

        Schema::create('subscriptions', function (Blueprint $table) {
           $table->bigInteger('user_id')->unsigned();
           $table->bigInteger('event_id')->unsigned();

           $table->foreign('user_id')
               ->references('id')->on('users')
               ->onDelete('cascade');
           $table->foreign('event_id')
               ->references('id')->on('events')
               ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
