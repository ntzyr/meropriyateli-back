<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Event::Class, 20)->create();

        // SPB Events
        factory(App\Event::Class, 20)->state('spb')->create();

        $interests = App\Interest::all();
        $events = App\Event::all();

        $events->each(function ($event) use($interests) {
            $event->interests()->attach(
                $interests->random(random_int(2,5))->pluck('id')->toArray()
            );
        });
    }
}
