<?php

use App\Event;
use App\Interest;
use App\User;
use Illuminate\Database\Seeder;

class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Event::Class, 10)->state('ended')->create();

        $interests = App\Interest::all();
        $events = App\Event::all();
        $users = App\User::all();

        $events->each(function($event) use($users, $interests){
            $event->subscribers()->attach(
                $users->random(random_int(1,5))->pluck('id')->toArray()
            );
            $event->interests()->attach(
                $interests->random(random_int(2,5))->pluck('id')->toArray()
            );
        });
    }
}
