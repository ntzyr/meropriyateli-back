<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create();

        $interests = App\Interest::all();
        $users = App\User::all();

        $users->each(function ($user) use($interests) {
            $user->interests()->attach(
                $interests->random(1,5)->pluck('id')->toArray()
            );
        });
        
    }
}
