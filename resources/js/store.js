import { getLocalUser } from './helpers/auth'

const user = getLocalUser()

export default {
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        events_filter: {
            filter: null,
            interests: [],
            price: [1000,2000],
            date_start: {},
            latitude: [],
            longitude: [],
        },
        map_data: {},
        events: null,
        markers: [],
        events_error: null
    },
    getters: {
        currentUser(state) {
            return state.currentUser
        },
        isLoggedIn(state) {
            return state.isLoggedIn
        },
        isLoading(state) {
            return state.loading
        },
        authError(state) {
            return state.auth_error
        },
        eventsFilter(state) {
            return state.events_filter
        },
        mapData(state) {
            return state.map_data
        },
        events(state) {
            return state.events
        },
        markers(state) {
            return state.markers
        }
    },
    mutations: {
        login(state) {
            state.loading = true
            state.auth_error = null
        },
        loginSuccess(state, payload) {
            state.auth_error = null
            state.isLoggedIn = true
            state.loading = false
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token})

            localStorage.setItem("user", JSON.stringify(state.currentUser))
        },
        loginFailed(state, payload) {
            state.auth_error = payload.error
            state.loading = false
        },
        logout(state) {
            localStorage.removeItem("user")
            state.isLoggedIn = false
            state.currentUser = null
        },
        eventsFilter(state) {
            state.loading = true
        },
        eventsFilterSuccess(state, payload) {
            state.loading = false
            state.events_filter = payload
        },
        setMapData(state) {
            state.loading = true
        },
        setMapDataSuccess(state, payload) {
            state.loading = false
            state.map_data = payload
        },
        getEvents(state, payload) {
            state.events = payload
        },
        getMarkers(state, payload) {
            state.markers = payload.data
        },
    },
    actions: {
        login(context) {
            context.commit("login")
        },
        eventsFilter(context) {
            context.commit("eventsFilter")
        },
        setMapData(context) {
            context.commit("setMapData")
        },
        events(context) {
            var rawFilters = context.getters.eventsFilter

            var filters = {
                filter: rawFilters.filter.value,
                price: rawFilters.price[0] + '-' + rawFilters.price[1],
                latitude: rawFilters.latitude[0] + ':' + rawFilters.latitude[1],
                longitude: rawFilters.longitude[0] + ':' + rawFilters.longitude[1],
                date_start: rawFilters.date_start.start + ',' + rawFilters.date_start.end
            }

            axios.get('/api/events', {
                params: filters
            })
                .then((response) => {
                    context.commit('getEvents', response.data);
                })
        },
        markers(context) {
            var rawFilters = context.getters.eventsFilter

            var filters = {
                filter: rawFilters.filter.value,
                price: rawFilters.price[0] + '-' + rawFilters.price[1],
                latitude: rawFilters.latitude[0] + ':' + rawFilters.latitude[1],
                longitude: rawFilters.longitude[0] + ':' + rawFilters.longitude[1],
                date_start: rawFilters.date_start.start + ',' + rawFilters.date_start.end
            }

            axios.get('/api/markers', {
                params: filters
            })
                .then((response) => {
                    context.commit('getMarkers', response.data);
                })
        }
    },
}
