import Events from './components/Events.vue'
import EventList from './components/event/List.vue'
import EventForm from './components/event/Form.vue'
import SingleEvent from './components/event/SingleEvent.vue'
import UserForm from './components/user/Form.vue'
import User from './components/User.vue'
import Admin from './components/Admin.vue'
import UserView from './components/user/View.vue'

export const routes = [
    {
        path: '/',
        component: Events,
        meta: {
            requiresAuth: false
        },
        children: [
            {
                path: '/',
                component: EventList,
                meta: {
                    requiresAuth: false
                }
            },
            {
                name: "event.new",
                path: '/event/new',
                component: EventForm,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/event/:slug',
                component: SingleEvent,
                meta: {
                    requiresAuth: false
                }
            },
            {
                path: '/event/:slug/edit',
                component: EventForm,
                meta: {
                    requiresAuth: true
                }
            },
        ]
    },
    {
        path: '/event',
        component: Events,

    },
    {
        path: '/user',
        component: User,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: ':id',
                component: UserView,
                meta: {
                    requiresAuth: false
                }
            },
            {
                path: ':id/edit',
                component: UserForm,
                meta: {
                    requiresAuth: true
                }
            }
        ]
    },
    {
        path: '/admin',
        component: Admin,
        meta: {
            requiresAuth: true
        }
    }
]
