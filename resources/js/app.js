require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'
import Vuetify from 'vuetify'
import Geocoder from "@pderas/vue2-geocoder";
import { routes } from './routes'
import StoreData from './store'
import 'babel-polyfill'
import * as VueGoogleMaps from 'vue2-google-maps'
import VuetifyDaterangePicker from "vuetify-daterange-picker"
import moment from 'moment'
import { initialize } from './helpers/general'

import App from './components/App.vue'

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(BootstrapVue)
Vue.use(Vuetify, {
    iconfont: 'fa'
})
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCFlvQSsCUec8yVB977i59AEHXo32rrljw',
    }
})
Vue.use(Geocoder, {
    defaultCountryCode: null, // e.g. 'CA'
    defaultLanguage:    null, // e.g. 'en'
    defaultMode:        'lat-lng', // or 'lat-lng'
    googleMapsApiKey:   'AIzaSyAOOjMBAaiuZHaayRah0xi77PxXmE1VAMU'
})
Vue.use(VuetifyDaterangePicker)

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(new Date(value)).format('D MMMM YYYY, dddd')
    }
})

const store = new Vuex.Store(StoreData)

const router = new VueRouter({
    routes,
    mode: 'history'
})

initialize(store, router)

const app = new Vue({
    el: '#site-app',
    router,
    store,
    components: { App }
});
