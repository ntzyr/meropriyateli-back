export function events(rawFilters)
{
    return new Promise((resolve, reject) => {
        // filter
        var filters = {
            filter: rawFilters.filter.value,
            price: rawFilters.price[0] + '-' + rawFilters.price[1],
            latitude: rawFilters.latitude[0] + ':' + rawFilters.latitude[1],
            longitude: rawFilters.longitude[0] + ':' + rawFilters.longitude[1],
            date_start: rawFilters.date_start.start + ',' + rawFilters.date_start.end
        }

        axios.get('/api/events',{
            params: filters
        })
            .then((response) => {
                resolve(response.data)
            })
            .catch((error) => {
                reject("No event found")
            })
    })
}

export function markers(rawFilters)
{
    return new Promise((resolve, reject) => {
        var filters = {
            filter: rawFilters.filter.value,
            price: rawFilters.price[0] + '-' + rawFilters.price[1],
            latitude: rawFilters.latitude[0] + ':' + rawFilters.latitude[1],
            longitude: rawFilters.longitude[0] + ':' + rawFilters.longitude[1],
            date_start: rawFilters.date_start.start + ',' + rawFilters.date_start.end
        }
        axios.get('/api/markers')
            .then((response) => {
                resolve(response.data)
            })
            .catch((error) => {
                reject("No markers found")
            })
    })
}
