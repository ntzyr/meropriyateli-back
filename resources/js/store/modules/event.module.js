export default {
    namespaced: true,
    state: {
        events: [],
        filters: {
            section: null,
            interests: [],
            price: [],
            date_start: {},
            latitude: [],
            longitude: []
        },
        error: null
    },
    getters: {
        events(state) {
            return state.events
        },
        filters(state) {
            return state.filters
        },
        error(state) {
            return state.error
        }
    },
    mutatuions: {

    },
    actions: {
        index(context){

        },
        show(context){

        },
        store(context){

        },
        destroy({context}, id){
            axios.delete(`/api/events/destroy/${id}`)
                .then()
                .catch()
        }
    }
}
