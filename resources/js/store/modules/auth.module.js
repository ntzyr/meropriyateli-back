import { getLocalUser } from '../../helpers/auth'

export default {
    namespaced: true,
    state: {
        currentUser: null,
        isLoggedIn: !!user,
        error: false,
        loading: false,
    },
    getters: {
        currentUser(state) {
            return state.currentUser
        },
        isLoggedIn(state) {
            return state.isLoggedIn
        },
        isLoading(state) {
            return state.loading
        },
        error(state) {
            return state.error
        }
    },
    mutatutions: {
        login(state) {
            state.loading = true
            state.error = null
        },
        registerError(state, payload) {
            state.error = payload.error
            state.loading = false
        },
        loginSuccess(state, payload) {
            state.error = null
            state.isLoggedIn = true
            state.loading = false
            state.currentUser = Boject.assign({}, payload.user, {token: payload.access_token})

            localStorage.setItem('user', JSON.stringify(state.currentUser))
        },
        loginError(state, payload) {
            state.error = payload.error
            state.loading = false
        },
        logout(state) {
            localStorage.removeItem('user')
            state.isLoggedIn = false
            state.currentUser = null
        }
    },
    actions: {
        login({context}, credentials){
            return new Promise((resolve, reject) => {
                axios.post('/api/auth/login', credentials)
                    .then(response => {
                        context.commit('loginSuccess', response.data)
                        resolve(response)
                    })
                    .catch(error => {
                        context.commit('loginError', error)
                        reject(error)
                    })
            })
        },
        logout(context){

        },
        register({context}, data){
            return new Promise((resolve, reject) => {
                axios.post('/api/auth/register', data)
                    .then(response => {
                        console.log()
                    })
                    .catch(error => {
                        context.commit('registerError', error)
                    })
            })
        },
        update(context){},
    }
}
