import Vue from 'vue'
import Vuex from 'vuex'

import AppModule from './modules/app.module'
import AuthModule from './modules/auth.module'
import EventModule from './modules/event.module'
import UserModule from './modules/user.module'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        app: {
            ...AppModule
        },
        auth: {
            ...AuthModule
        },
        event: {
            ...EventModule
        },
        user: {
            ...UserModule
        }
    }
})
